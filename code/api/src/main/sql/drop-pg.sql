drop table if exists departments CASCADE ;
drop table if exists departments_users CASCADE ;
drop table if exists faculties CASCADE ;
drop table if exists faculties_users CASCADE ;
drop table if exists project_documents CASCADE ;
drop table if exists projects CASCADE ;
drop table if exists users CASCADE ;
