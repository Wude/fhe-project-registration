create table departments (department_acronym char(3) not null, creation_instant timestamp not null, modification_instant timestamp not null, name varchar(255) not null, faculty_acronym char(3) not null, primary key (department_acronym));
create table departments_users (departments_department_acronym char(3) not null, users_user_id binary not null);
create table faculties (faculty_acronym char(3) not null, creation_instant timestamp not null, modification_instant timestamp not null, name varchar(255) not null, primary key (faculty_acronym));
create table faculties_users (faculties_faculty_acronym char(3) not null, users_user_id binary not null);
create table project_documents (project_document_id binary not null, content binary(255) not null, creation_instant timestamp not null, modification_instant timestamp not null, title varchar(255) not null, project_id binary not null, primary key (project_document_id));
create table projects (project_id binary not null, creation_instant timestamp not null, deadline date not null, grade decimal(2,1), modification_instant timestamp not null, status char(1) not null, student_signature binary(255), supervising_tutor_1_accepted boolean not null, supervising_tutor_1_signature binary(255), supervising_tutor_2_accepted boolean not null, supervising_tutor_2_signature binary(255), title varchar(255) not null, student_id binary not null, supervising_tutor_1_id binary not null, supervising_tutor_2_id binary, primary key (project_id));
create table users (user_id binary not null, admin boolean not null, creation_instant timestamp not null, email varchar(254) not null, examination_authority boolean not null, faculty_board_of_examiners boolean not null, modification_instant timestamp not null, password varchar(60) not null, user_type char(3) not null, primary key (user_id));
alter table users add constraint uk_user_email unique (email);
alter table departments add constraint fk_department_f_acronym foreign key (faculty_acronym) references faculties;
alter table departments_users add constraint FKlnkw3igx15mq05yos16tc4abk foreign key (users_user_id) references users;
alter table departments_users add constraint FKflc1n41arid456x7y5hr7005m foreign key (departments_department_acronym) references departments;
alter table faculties_users add constraint FKh59l2lpv9gys99670lckogli foreign key (users_user_id) references users;
alter table faculties_users add constraint FK68bc2snfdy9d3bbg3tgi48oe9 foreign key (faculties_faculty_acronym) references faculties;
alter table project_documents add constraint fk_project_document_p_id foreign key (project_id) references projects;
alter table projects add constraint fk_project_student_id foreign key (student_id) references users;
alter table projects add constraint fk_project_tutor_1_id foreign key (supervising_tutor_1_id) references users;
alter table projects add constraint fk_project_tutor_2_id foreign key (supervising_tutor_2_id) references users;
