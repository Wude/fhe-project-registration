insert into users (
    user_id, creation_instant, modification_instant, email, password,
    user_type, admin, faculty_board_of_examiners, examination_authority)
values (
    '612bc797-83ef-efa7-a782-775746567316', now(), now(),
    'stefan.woyde@fh-erfurt.de',
    '2a$10$loNsdcdGqC7zkYg0bW7P6.zKpbui4RqyaLYHIMnPWAwZJQ.pbSmFW',
    'STU', true, false, false);

insert into users (
    user_id, creation_instant, modification_instant, email, password,
    user_type, admin, faculty_board_of_examiners, examination_authority)
values (
    '612d3ea1-a0f0-f0a7-a7fc-775746569316', now(), now(),
    'steffen.avemarg@fh-erfurt.de',
    '2a$10$loNsdcdGqC7zkYg0bW7P6.zKpbui4RqyaLYHIMnPWAwZJQ.pbSmFW',
    'LEC', true, true, false);
