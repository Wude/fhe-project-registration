package de.fhe.proreg.data

import de.fhe.proreg.Util
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
import java.time.Instant
import java.util.UUID
import javax.persistence.Cacheable
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ForeignKey
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToOne
import javax.persistence.ManyToMany
import javax.persistence.OneToMany
import javax.persistence.PrePersist
import javax.persistence.Table
import javax.enterprise.context.ApplicationScoped

@Entity
@Cacheable
@Table(name = "departments")
open class Department {

    @Id
    @Column(name = "department_acronym", columnDefinition = "char(3)", nullable = false, updatable = false)
    open var acronym: String? = null

    @Column(name = "creation_instant", nullable = false)
    lateinit open var creationInstant: Instant

    @Column(name = "modification_instant", nullable = false)
    lateinit open var modificationInstant: Instant

    @Column(name = "name", nullable = false)
    lateinit open var name: String

    @ManyToOne(optional = false, cascade = [CascadeType.ALL])
    @JoinColumn(name = "faculty_acronym", nullable = false, foreignKey = ForeignKey(name = "fk_departments_f_acronym"))
    lateinit open var faculty: Faculty

    @OneToMany(targetEntity = Project::class, orphanRemoval = true, mappedBy = "department")
    lateinit open var projects: List<Project>

    @ManyToMany(targetEntity = User::class, cascade = [CascadeType.ALL])
    // @JoinTable(name = "user_department",
    //     inverseForeignKey = ForeignKey(name = "fk_user_department_d_acronym"))
    lateinit open var users: List<User>

    @PrePersist
    fun onPrePersist() {
        if (creationInstant.equals(Util.EpochStartInstant)) {
            creationInstant = Instant.now()
        }
        modificationInstant = Instant.now()
    }

}

@ApplicationScoped
class DepartmentRepository : PanacheRepositoryBase<Department, String> {

}
