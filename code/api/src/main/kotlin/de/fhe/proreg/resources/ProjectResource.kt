package de.fhe.proreg.resources

import de.fhe.proreg.Util
import de.fhe.proreg.RestStatusInt
import de.fhe.proreg.RestStatusString
import de.fhe.proreg.data.Project
import de.fhe.proreg.data.ProjectStatus
import de.fhe.proreg.data.UserGroup
import de.fhe.proreg.data.UserType
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.models.BaseResponse
import de.fhe.proreg.models.ProjectViewResponse
import de.fhe.proreg.models.ProjectListResponse
import de.fhe.proreg.models.ProjectCreationRequest
import de.fhe.proreg.models.ProjectDocumentCreationRequest
import de.fhe.proreg.services.ProjectService
import de.fhe.proreg.services.UserService
import io.smallrye.jwt.build.Jwt
import java.io.IOException
import java.io.InputStream
import java.io.ByteArrayOutputStream
import java.util.Base64
import java.util.UUID
import javax.annotation.security.PermitAll
import javax.annotation.security.RolesAllowed
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.context.RequestScoped
import javax.inject.Inject
import javax.transaction.Transactional
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.FormParam
import javax.ws.rs.Produces
import javax.ws.rs.POST
import javax.ws.rs.PUT
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claim
import org.eclipse.microprofile.jwt.Claims
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Counted
import org.eclipse.microprofile.metrics.annotation.Gauge
import org.eclipse.microprofile.metrics.annotation.Timed
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.jboss.logging.Logger
import org.jboss.resteasy.annotations.providers.multipart.PartType

@Path("/api/project")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
open class ProjectResource : ResourceBase() {

    @Inject
    lateinit var jwt: JsonWebToken

    @Inject
    lateinit var userService: UserService

    @Inject
    lateinit var projectService: ProjectService

    @POST
    @Transactional
    @RolesAllowed(UserGroup.STUDENT)
    @Path("/create")
    @Counted(name = "projectCreateCount", description = "Project create count")
    @Timed(name = "projectCreateTime", description = "Project create handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Create a new project",
        description = "Create a new project with the given contents")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_CREATED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = "Project contents invalid",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only a student may create a new project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun rejectProject(request: ProjectCreationRequest): Response {
        val reason = projectService.createProject(
                        request.studentID,
                        request.supervisingTutor1ID,
                        request.supervisingTutor2ID,
                        request.title,
                        Base64Decoder.decode(request.studentSignature))

        return if (reason == null) { createResponse(RestStatusInt.OK_200, PROJECT_CREATED) }
            else { createResponse(RestStatusInt.BAD_REQUEST_400, reason) }
    }

    @GET
    @Transactional
    @RolesAllowed(
        UserGroup.STUDENT,
        UserGroup.LECTURER,
        UserGroup.FACULTY_BOARD_OF_EXAMINERS,
        UserGroup.FACULTY_SECRETARIAT_STAFF,
        UserGroup.EXAMINATION_AUTHORITY_STAFF,
        UserGroup.ADMINISTRATOR)
    @Path("/{projectID}")
    @Counted(name = "projectViewCount", description = "Project view count")
    @Timed(name = "projectViewTime", description = "Project view handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "View a project",
        description = "View the project with the given id")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT, implementation = ProjectViewResponse::class))]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only a valid user may view this project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun viewProject(@PathParam("projectID") projectID: UUID): Response {
        val userID = UUID.fromString(jwt.subject)
        val (project, reason) = projectService.viewProject(projectID, userID, jwt.groups)

        return if (project != null) { createResponse(toData(project), RestStatusInt.OK_200, PROJECT_FOUND) }
            else { createResponse(RestStatusInt.BAD_REQUEST_400, reason ?: PROJECT_NOT_EXISTS) }
    }

    @GET
    @Transactional
    @RolesAllowed(UserGroup.STUDENT, UserGroup.LECTURER)
    @Path("/related")
    @Counted(name = "projectListRelatedCount", description = "Project list related count")
    @Timed(name = "projectListRelatedTime", description = "Project list related handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List related projects",
        description = "List related projects")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT, implementation = ProjectListResponse::class))]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only a student or lecturer may list related projects",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listProjects(): Response {
        val userID = UUID.fromString(jwt.subject)
        var projects = projectService.viewProjectsByUser(userID, jwt.groups)
        val response = toData(projects)
        return createResponse(response, RestStatusInt.OK_200)
    }

    @GET
    @Transactional
    @RolesAllowed(UserGroup.FACULTY_SECRETARIAT_STAFF)
    @Path("/accepted/faculty/{facultyAcronym}")
    @Counted(name = "projectListAcceptedCount", description = "Project list accepted count")
    @Timed(name = "projectListAcceptedTime", description = "Project list accepted handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List accepted projects",
        description = "List accepted projects")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT, implementation = ProjectListResponse::class))]),
        APIResponse(responseCode = RestStatusString.NO_CONTENT_204, description = PROJECT_NOT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only a secreteriat staff member may list accepted projects",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listAcceptedProjects(@PathParam("facultyAcronym") facultyAcronym: String): Response {
        var projects = projectService.viewProjectsByStatus(ProjectStatus.ACCEPTED, facultyAcronym)
        val response = toData(projects)

        return if (projects.size == 0) { createResponse(response, RestStatusInt.OK_200, PROJECT_FOUND) }
            else { createResponse(response, RestStatusInt.NO_CONTENT_204, PROJECT_NOT_FOUND) }
    }

    @GET
    @Transactional
    @RolesAllowed(UserGroup.FACULTY_BOARD_OF_EXAMINERS)
    @Path("/deadline-extension-requested/faculty/{facultyAcronym}")
    @Counted(name = "projectListDeadlineExtensionRequestedCount", description = "Project list deadline extension requested count")
    @Timed(name = "projectListDeadlineExtensionRequestedTime", description = "Project list deadline extension requested handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List deadline extension requested projects",
        description = "List deadline extension requested projects")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.NO_CONTENT_204, description = PROJECT_NOT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a member of the board of examiners may list projects for which a deadline extension was requested",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listDeadlineExtensionRequestedProjects(@PathParam("facultyAcronym") facultyAcronym: String): Response {
        var projects = projectService.viewProjectsByStatus(ProjectStatus.DEADLINE_EXTENSION_REQUESTED, facultyAcronym)
        val response = toData(projects)

        return if (projects.size == 0) { createResponse(response, RestStatusInt.OK_200, PROJECT_FOUND) }
            else { createResponse(response, RestStatusInt.NO_CONTENT_204, PROJECT_NOT_FOUND) }
    }

    @GET
    @Transactional
    @RolesAllowed(UserGroup.EXAMINATION_AUTHORITY_STAFF)
    @Path("/graded")
    @Counted(name = "projectListGradedRequestedCount", description = "Project list graded count")
    @Timed(name = "projectListGradedRequestedTime", description = "Project list graded handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List graded projects",
        description = "List graded projects")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT, implementation = ProjectListResponse::class))]),
        APIResponse(responseCode = RestStatusString.NO_CONTENT_204, description = PROJECT_NOT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only a member of the board of examiners may list accepted projects",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listGradedProjects(): Response {
        var projects = projectService.viewProjectsByStatus(ProjectStatus.GRADED)
        val response = toData(projects)

        return if (projects.size == 0) { createResponse(response, RestStatusInt.OK_200, PROJECT_FOUND) }
            else { createResponse(response, RestStatusInt.NO_CONTENT_204, PROJECT_NOT_FOUND) }
    }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.LECTURER)
    @Path("/{projectID}/reject")
    @Counted(name = "projectRejectCount", description = "Project reject count")
    @Timed(name = "projectRejectTime", description = "Project reject handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Reject a project",
        description = "Reject a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_REJECTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only a lecturer may reject a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun rejectProject(@PathParam("projectID") projectID: UUID): Response =
        if (projectService.changeProjectStatus(projectID, ProjectStatus.REJECTED)) {
            createResponse(RestStatusInt.OK_200, PROJECT_REJECTED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
        }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.LECTURER)
    @Path("/{projectID}/accept")
    @Counted(name = "projectAcceptCount", description = "Project accept count")
    @Timed(name = "projectAcceptTime", description = "Project accept handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Accept a project",
        description = "Accept a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_ACCEPTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only a lecturer may accept a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun acceptProject(@PathParam("projectID") projectID: UUID): Response =
        if (projectService.changeProjectStatus(projectID, ProjectStatus.REJECTED)) {
            createResponse(RestStatusInt.OK_200, PROJECT_ACCEPTED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
        }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.FACULTY_SECRETARIAT_STAFF)
    @Path("/{projectID}/register")
    @Counted(name = "projectRegisterCount", description = "Project register count")
    @Timed(name = "projectRegisterTime", description = "Project register handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Register a project",
        description = "Register a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_REGISTERED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only the secreteriat staff may register a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun registerProject(@PathParam("projectID") projectID: UUID): Response =
        if (projectService.changeProjectStatus(projectID, ProjectStatus.REGISTERED)) {
            createResponse(RestStatusInt.OK_200, PROJECT_REGISTERED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
        }

    @POST
    @Transactional
    @RolesAllowed(UserGroup.STUDENT)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/{projectID}/document/create")
    @Counted(name = "projectDocumentCreateCount", description = "Project document create count")
    @Timed(name = "projectDocumentCreateTime", description = "Project document create handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Create a new project document",
        description = "Create a new project document")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_REGISTERED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only the secreteriat staff may register a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun createProjectDocument(@PathParam("projectID") projectID: UUID, request: ProjectDocumentCreationRequest): Response =
        if (projectService.createProjectDocument(projectID, request.title, getByteArray(request.content))) {
            createResponse(RestStatusInt.OK_200, PROJECT_REGISTERED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
        }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.STUDENT)
    @Path("/{projectID}/deadline-extension/request")
    @Counted(name = "projectDeadlineExtensionRequestCount", description = "Project deadline extension request count")
    @Timed(name = "projectDeadlineExtensionRequestTime", description = "Project deadline extension request handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Request a deadline extension for a project",
        description = "Request the deadline extension for a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_DEADLINE_EXTENSION_REQUESTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a student may request a deadline extension for a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun requestDeadlineExtension(@PathParam("projectID") projectID: UUID): Response =
        if (projectService.changeProjectStatus(projectID, ProjectStatus.DEADLINE_EXTENSION_REQUESTED)) {
            createResponse(RestStatusInt.OK_200, PROJECT_DEADLINE_EXTENSION_REQUESTED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
        }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.FACULTY_BOARD_OF_EXAMINERS)
    @Path("/{projectID}/deadline-extension/reject")
    @Counted(name = "projectDeadlineExtensionRejectCount", description = "Project deadline extension reject count")
    @Timed(name = "projectDeadlineExtensionRejectTime", description = "Project deadline extension reject handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Reject the deadline extension of a project",
        description = "Reject the deadline extension of a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_DEADLINE_EXTENSION_REJECTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a member of the board of examiners may reject the deadline extension of a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun rejectDeadlineExtension(@PathParam("projectID") projectID: UUID): Response =
        if (projectService.changeProjectStatus(projectID, ProjectStatus.DEADLINE_EXTENSION_REJECTED)) {
            createResponse(RestStatusInt.OK_200, PROJECT_DEADLINE_EXTENSION_REJECTED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
        }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.FACULTY_BOARD_OF_EXAMINERS)
    @Path("/{projectID}/deadline-extension/grant")
    @Counted(name = "projectDeadlineExtensionGrantCount", description = "Project deadline extension grant count")
    @Timed(name = "projectDeadlineExtensionGrantTime", description = "Project deadline extension grant handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Grant the deadline extension of a project",
        description = "Grant the deadline extension of a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_DEADLINE_EXTENSION_GRANTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a member of the board of examiners may grant the deadline extension of a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun grantDeadlineExtension(@PathParam("projectID") projectID: UUID): Response =
        if (projectService.changeProjectStatus(projectID, ProjectStatus.DEADLINE_EXTENSION_GRANTED)) {
            createResponse(RestStatusInt.OK_200, PROJECT_DEADLINE_EXTENSION_GRANTED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
        }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.EXAMINATION_AUTHORITY_STAFF)
    @Path("/{projectID}/record-grading")
    @Counted(name = "projectRecordGradingCount", description = "Project record grading count")
    @Timed(name = "projectRecordGradingTime", description = "Project record grading handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Record the grading of a project",
        description = "Record the grading of a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_GRADING_RECORDED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only the examination authority may record the grading of a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun recordGrading(@PathParam("projectID") projectID: UUID): Response =
        if (projectService.changeProjectStatus(projectID, ProjectStatus.COMPLETED)) {
            createResponse(RestStatusInt.OK_200, PROJECT_GRADING_RECORDED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
        }

    companion object {

        private fun toData(projects: List<Project>?): ProjectListResponse =
            if (projects != null) {
                val projectViews = mutableListOf<ProjectViewResponse>()
                for (project in projects) {
                    projectViews.add(toData(project))
                }
                ProjectListResponse(projectViews)
            } else {
                ProjectListResponse(listOf<ProjectViewResponse>())
            }

        private fun toData(project: Project) = ProjectViewResponse(
            project.projectID!!,
            project.student.userID!!,
            project.creationInstant,
            project.modificationInstant,
            project.supervisingTutor1.userID!!,
            project.supervisingTutor2?.userID,
            project.title,
            project.grade,
            project.deadline,
            project.supervisingTutor1Accepted,
            project.supervisingTutor2Accepted,
            Base64Encoder.encodeToString(project.studentSignature),
            Base64Encoder.encodeToString(project.supervisingTutor1Signature),
            Base64Encoder.encodeToString(project.supervisingTutor2Signature))

        private const val PROJECT_NOT_EXISTS = "A project with the given id does not exist"
        private const val PROJECT_NOT_FOUND = "No projects found"
        private const val PROJECT_FOUND = "Project found"
        private const val PROJECT_CREATED = "Project created"
        private const val PROJECT_ACCEPTED = "Project accepted"
        private const val PROJECT_REJECTED = "Project rejected"
        private const val PROJECT_REGISTERED = "Project registered"
        private const val PROJECT_GRADING_RECORDED = "Project grading recorded"
        private const val PROJECT_DEADLINE_EXTENSION_REQUESTED = "Project deadline extension requested"
        private const val PROJECT_DEADLINE_EXTENSION_GRANTED = "Project deadline extension granted"
        private const val PROJECT_DEADLINE_EXTENSION_REJECTED = "Project deadline extension rejected"

        private val Base64Encoder = Base64.getEncoder()
        private val Base64Decoder = Base64.getDecoder()

        private val LOG: Logger = Logger.getLogger(ProjectResource::class.java)
    }
}
