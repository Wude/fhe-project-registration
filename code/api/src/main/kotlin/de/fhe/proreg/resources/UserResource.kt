package de.fhe.proreg.resources

import de.fhe.proreg.RestStatusInt
import de.fhe.proreg.RestStatusString
import de.fhe.proreg.data.UserGroup
import de.fhe.proreg.data.UserType
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.models.UserCreationRequest
import de.fhe.proreg.models.UserLoginRequest
import de.fhe.proreg.models.UserLoginResponse
import de.fhe.proreg.models.UserPasswordChangeRequest
import de.fhe.proreg.services.UserService
import io.smallrye.jwt.build.Jwt
import javax.annotation.security.PermitAll
import javax.annotation.security.RolesAllowed
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.context.RequestScoped
import javax.inject.Inject
import javax.transaction.Transactional
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.POST
import javax.ws.rs.PUT
import javax.ws.rs.core.Context
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claim
import org.eclipse.microprofile.jwt.Claims
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Counted
import org.eclipse.microprofile.metrics.annotation.Gauge
import org.eclipse.microprofile.metrics.annotation.Timed
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.jboss.logging.Logger
import org.jose4j.jwt.JwtClaims

@Path("/api/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
open class UserResource : ResourceBase() {

    @Inject
    lateinit var jwt: JsonWebToken

    @Inject
    lateinit var userService: UserService

    @POST
    @Transactional
    @RolesAllowed(UserGroup.ADMINISTRATOR)
    @Path("/create")
    @Counted(name = "userCreateCount", description = "User create count")
    @Timed(name = "userCreateTime", description = "User create handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Create a new user",
        description = "Create a new user for the project registration")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.CREATED_201, description = USER_CREATED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = USER_ALREADY_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun createUser(request: UserCreationRequest): Response =
        if (userService.createUser(
                request.email,
                request.password,
                request.userType,
                request.admin,
                request.facultyBoardOfExaminers,
                request.examinationAuthority,
                request.facultyAcronyms,
                request.departmentAcronyms
        )) {
            LOG.info("User ${request.email} created")
            createResponse(RestStatusInt.CREATED_201, USER_CREATED)
        } else {
            LOG.warn("User ${request.email} already exists")
            createResponse(RestStatusInt.BAD_REQUEST_400)
        }

    @PUT
    @Transactional
    @RolesAllowed(
        UserGroup.STUDENT,
        UserGroup.LECTURER,
        UserGroup.FACULTY_BOARD_OF_EXAMINERS,
        UserGroup.FACULTY_SECRETARIAT_STAFF,
        UserGroup.EXAMINATION_AUTHORITY_STAFF,
        UserGroup.ADMINISTRATOR)
    @Path("/changePassword")
    @Counted(name = "userChangePasswordCount", description = "User change password count")
    @Timed(name = "userChangePasswordTime", description = "User change password handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Change password",
        description = "Change the password of the calling user")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.CREATED_201, description = USER_CREATED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = USER_ALREADY_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "A non-user cannot change password",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun changePassword(request: UserPasswordChangeRequest): Response {
        val userID = jwt.getClaim<String>(Claims.sub)
        val email = jwt.getClaim<String>(Claims.email)

        return if (userService.changePassword(userID, request.password)) {
            LOG.info("User ${email} changed password")
            createResponse(RestStatusInt.CREATED_201, USER_CREATED)
        } else {
            LOG.warn("User ${email} password change attempt failed")
            createResponse(RestStatusInt.BAD_REQUEST_400)
        }
    }

    @POST
    @Transactional
    @PermitAll
    @Path("/login")
    @Counted(name = "loginCount", description = "Login attempt count")
    @Timed(name = "loginTime", description = "Login attempt handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Attempt to log into the project registration",
        description = "Attempt to log into the project registration")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = LOGIN_SUCCESSFUL,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT, implementation = UserLoginResponse::class))]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = LOGIN_FAILED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun login(request: UserLoginRequest): Response {
        val (userID, token) = userService.createToken(request.email, request.password)

        return if (userID != null && token != null) {
            LOG.info("User ${request.email} logged in")
            createResponse(UserLoginResponse(userID, token), RestStatusInt.OK_200, LOGIN_SUCCESSFUL)
        } else {
            LOG.warn("User ${request.email} failed to login")
            createResponse(RestStatusInt.UNAUTHORIZED_401, LOGIN_FAILED)
        }
    }

    @POST
    @RolesAllowed(
        UserGroup.STUDENT,
        UserGroup.LECTURER,
        UserGroup.FACULTY_BOARD_OF_EXAMINERS,
        UserGroup.FACULTY_SECRETARIAT_STAFF,
        UserGroup.EXAMINATION_AUTHORITY_STAFF,
        UserGroup.ADMINISTRATOR)
    @Path("/logout")
    @Counted(name = "logoutCount", description = "Logout count")
    @Timed(name = "logoutTime", description = "Logout handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Log out of the project registration",
        description = "Log out of the project registration")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = LOGOUT_SUCCESSFUL,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Logging out requires to be logged in",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun logout(): Response {
        userService.revokeToken(jwt.rawToken)
        return createResponse(RestStatusInt.OK_200, LOGOUT_SUCCESSFUL)
    }

    @GET
    @PermitAll
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    @Counted(name = "helloCount", description = "How many requests on the Hello resource have been performed.")
    @Timed(name = "helloTime", description = "A measure of how long it takes to handle the Hello request.", unit = MetricUnits.MILLISECONDS)
    fun hello() = "Hello project registration!"

    @GET
    @RolesAllowed(UserGroup.STUDENT)
    @Path("/test")
    @Produces(MediaType.TEXT_PLAIN)
    @Counted(name = "testCount", description = "How many requests on the Test resource have been performed.")
    @Timed(name = "testTime", description = "A measure of how long it takes to handle the Test request.", unit = MetricUnits.MILLISECONDS)
    fun test(): String {
        return "Test project registration!"
    }

    companion object {
        const val USER_CREATED = "User created"
        const val USER_ALREADY_EXISTS = "A user with the given email already exists"
        const val LOGIN_SUCCESSFUL = "Login successful"
        const val LOGIN_FAILED = "Email or password invalid"
        const val LOGOUT_SUCCESSFUL = "Logout successful"

        private val LOG: Logger = Logger.getLogger(UserResource::class.java)
    }
}
