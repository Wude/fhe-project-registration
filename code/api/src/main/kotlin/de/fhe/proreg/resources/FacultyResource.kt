package de.fhe.proreg.resources

import de.fhe.proreg.RestStatusInt
import de.fhe.proreg.RestStatusString
import de.fhe.proreg.data.UserGroup
import de.fhe.proreg.data.UserType
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.services.UserService
import de.fhe.proreg.services.FacultyService
import de.fhe.proreg.models.FacultyCreationRequest
import de.fhe.proreg.models.DepartmentCreationRequest
import io.smallrye.jwt.build.Jwt
import javax.annotation.security.PermitAll
import javax.annotation.security.RolesAllowed
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.POST
import javax.ws.rs.Produces
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claim
import org.eclipse.microprofile.jwt.Claims
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Counted
import org.eclipse.microprofile.metrics.annotation.Gauge
import org.eclipse.microprofile.metrics.annotation.Timed
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.jboss.logging.Logger

@Path("/api/faculty")
@RolesAllowed(UserGroup.ADMINISTRATOR)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
open class FacultyResource : ResourceBase() {

    @Inject
    lateinit var userService: UserService

    @Inject
    lateinit var facultyService: FacultyService

    @POST
    @Path("/create")
    @Counted(name = "facultyCreateCount", description = "Faculty create count")
    @Timed(name = "facultyCreateTime", description = "Faculty create handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Create a faculty",
        description = "Create a faculty with an acronym and a name")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = FACULTY_CREATED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = FACULTY_ALREADY_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only an admin may create a faculty",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun createFaculty(request: FacultyCreationRequest): Response =
        if (facultyService.createFaculty(request.facultyAcronym, request.facultyName)) {
            createResponse(RestStatusInt.CREATED_201, FACULTY_CREATED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, FACULTY_ALREADY_EXISTS)
        }

    @POST
    @Path("/department/create")
    @Counted(name = "departmentCreateCount", description = "Department create count")
    @Timed(name = "departmentCreateTime", description = "Department create handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Create a department",
        description = "Create a department with an acronym and a name")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = DEPARTMENT_CREATED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = DEPARTMENT_ALREADY_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only an admin may create a department",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun createFaculty(request: DepartmentCreationRequest): Response =
        if (facultyService.createDepartment(request.facultyAcronym, request.departmentAcronym, request.departmentName)) {
            createResponse(RestStatusInt.CREATED_201, DEPARTMENT_CREATED)
        } else {
            createResponse(RestStatusInt.BAD_REQUEST_400, DEPARTMENT_ALREADY_EXISTS)
        }

    companion object {
        const val FACULTY_CREATED = "Faculty created"
        const val FACULTY_ALREADY_EXISTS = "A faculty with the given acronym already exists"

        const val DEPARTMENT_CREATED = "Department created"
        const val DEPARTMENT_ALREADY_EXISTS = "A department with the given acronym already exists"

        private val LOG: Logger = Logger.getLogger(FacultyResource::class.java)
    }
}
