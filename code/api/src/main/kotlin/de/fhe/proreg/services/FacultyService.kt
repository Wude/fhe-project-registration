package de.fhe.proreg.services

import de.fhe.proreg.data.Department
import de.fhe.proreg.data.DepartmentRepository
import de.fhe.proreg.data.Faculty
import de.fhe.proreg.data.FacultyRepository
import io.smallrye.jwt.build.Jwt
import io.quarkus.redis.client.reactive.ReactiveRedisClient
import io.smallrye.mutiny.Uni
import io.vertx.mutiny.redis.client.Response
import java.time.Instant
import java.time.Duration
import javax.inject.Inject
import javax.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claims
import org.jboss.logging.Logger

/**
* Service for handling faculties and departments
*/
@ApplicationScoped
open class FacultyService {

    @Inject
    lateinit var facultyRepository: FacultyRepository

    @Inject
    lateinit var departmentRepository: DepartmentRepository

    fun createFaculty(facultyAcronym: String, facultyName: String): Boolean {
        var faculty = facultyRepository.findById(facultyAcronym)
        return if (faculty == null) {
            false
        } else {
            faculty = Faculty()
            faculty.acronym = facultyAcronym
            faculty.name = facultyName
            facultyRepository.persist(faculty)

            true
        }
    }

    fun createDepartment(facultyAcronym: String, departmentAcronym: String, departmentName: String): Boolean {
        val faculty = facultyRepository.findById(facultyAcronym)
        return if (faculty == null) {
            false
        } else {
            var department = departmentRepository.findById(departmentAcronym)
            if (department == null) {
                false
            } else {
                department = Department()
                department.acronym = departmentAcronym
                department.name = departmentName
                department.faculty = faculty
                departmentRepository.persist(department)

                true
            }
        }
    }

    companion object {
        private val LOG: Logger = Logger.getLogger(FacultyService::class.java)
    }
}
