package de.fhe.proreg.data

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
import io.quarkus.elytron.security.common.BcryptUtil
import java.time.Instant
import java.util.UUID
import javax.persistence.AttributeConverter
import javax.persistence.Cacheable
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Converter
import javax.persistence.Entity
import javax.persistence.ForeignKey
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinTable
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.ManyToMany
import javax.persistence.PrePersist
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import javax.enterprise.context.ApplicationScoped
import org.jboss.logging.Logger
import org.wildfly.security.password.Password
import org.wildfly.security.password.PasswordFactory
import org.wildfly.security.password.interfaces.BCryptPassword
import org.wildfly.security.password.util.ModularCrypt

/**
* The groups a user may belong to.
*/
object UserGroup {

    /**
    * A student.
    */
    const val STUDENT = "STU"

    /**
    * A lecturer.
    */
    const val LECTURER = "LEC"

    /**
    * A lecturer who is also a member of the faculty board of examiners.
    */
    const val FACULTY_BOARD_OF_EXAMINERS = "FBE"

    /**
    * A staff member of the faculty secreteriat.
    */
    const val FACULTY_SECRETARIAT_STAFF = "FSS"

    /**
    * A staff member of the examination authority.
    */
    const val EXAMINATION_AUTHORITY_STAFF = "EAS"

    /**
    * An administrator.
    */
    const val ADMINISTRATOR = "ADM"
}

/**
* The user types.
*/
enum class UserType(val code: String) {

    /**
    * A student.
    */
    STUDENT(UserGroup.STUDENT),

    /**
    * A lecturer.
    */
    LECTURER(UserGroup.LECTURER),

    /**
    * A staff member of the faculty secreteriat.
    */
    FACULTY_SECRETARIAT_STAFF(UserGroup.FACULTY_SECRETARIAT_STAFF),

    /**
    * A staff member of the examination authority.
    */
    EXAMINATION_AUTHORITY_STAFF(UserGroup.EXAMINATION_AUTHORITY_STAFF),

    /**
    * An administration staff member.
    */
    ADMINISTRATOR_STAFF(UserGroup.ADMINISTRATOR);

    companion object {

        val valuesByCode = HashMap<String, UserType>()

        init {
            val userTypeValues = UserType.values()

            for (userType in userTypeValues) {
                valuesByCode.put(userType.code, userType);
                valuesByCode.put(userType.name, userType);
            }
        }

        fun tryValueOf(idOrCode: String?, defaultValue: UserType?): UserType? = valuesByCode.get(idOrCode) ?: defaultValue
        fun tryValueOf(idOrCode: String?): UserType? = tryValueOf(idOrCode, null)
    }
}

@Converter(autoApply = true)
class UserTypeConverter : AttributeConverter<UserType, String> {

    override fun convertToDatabaseColumn(userType: UserType?): String? = if (userType == null) { null } else { userType.code }
    override fun convertToEntityAttribute(code: String?): UserType? = UserType.tryValueOf(code)
}

/**
* A user.
*/
@Entity
@Cacheable
@Table(name = "users", uniqueConstraints = [UniqueConstraint(name = "uk_users_email", columnNames = ["email"])])
open class User {

    @Id
    // @GeneratedValue
    // @GeneratedValue(strategy = GenerationType.)
    // @GeneratedValue(generator = "UUID")
    // @GenericGenerator(
    //     name = “UUID”,
    //     strategy = “org.hibernate.id.UUIDGenerator”,
    // )
    // @Column(name = "user_id", columnDefinition = "uuid", nullable = false, updatable = false)
    @Column(name = "user_id", nullable = false, updatable = false)
    open var userID: UUID? = null

    @Column(name = "password", nullable = false, length = 60)
    lateinit open var password: String

    @Column(name = "creation_instant", nullable = false)
    lateinit open var creationInstant: Instant

    @Column(name = "modification_instant", nullable = false)
    lateinit open var modificationInstant: Instant

    /**
    * Email address
    * Maximum length: https://www.rfc-editor.org/errata_search.php?rfc=3696&eid=1690
    */
    @Column(name = "email", nullable = false, length = 254)
    lateinit open var email: String

    @Convert(converter = UserTypeConverter::class)
    @Column(name = "user_type", columnDefinition = "char(3)", nullable = false)
    lateinit open var userType: UserType

    @Column(name = "admin", nullable = false)
    open var admin: Boolean = false

    @Column(name = "faculty_board_of_examiners", nullable = false)
    open var facultyBoardOfExaminers: Boolean = false

    @Column(name = "examination_authority", nullable = false)
    open var examinationAuthority: Boolean = false

    @ManyToMany(targetEntity = Faculty::class, cascade = [CascadeType.ALL], mappedBy = "users")
    // @ManyToMany(targetEntity = Faculty::class, cascade = [CascadeType.ALL])
    // @JoinTable(name = "user_faculty",
    //     joinColumns = [JoinColumn(name = "user_id")],
    //     inverseJoinColumns = [JoinColumn(name = "faculty_acronym")],
    //     inverseForeignKey = ForeignKey(name = "fk_users_faculty_u_id"))
    lateinit open var faculties: List<Faculty>

    @ManyToMany(targetEntity = Department::class, cascade = [CascadeType.ALL], mappedBy = "users")
    // @ManyToMany(targetEntity = Department::class, cascade = [CascadeType.ALL])
    // @JoinTable(name = "user_department",
    //     joinColumns = [JoinColumn(name = "user_id")],
    //     inverseJoinColumns = [JoinColumn(name = "department_acronym")],
    //     inverseForeignKey = ForeignKey(name = "fk_users_department_u_id"))
    lateinit open var departments: List<Department>

    @OneToMany(targetEntity = Project::class, orphanRemoval = true, mappedBy = "student")
    lateinit open var projects: List<Project>

    @PrePersist
    fun onPrePersist() {
        if (userID == null) {
            userID = UUID.randomUUID()
            creationInstant = Instant.now()
        }
        modificationInstant = Instant.now()
    }

    fun changePassword(plainPassword: String) {
        password = BcryptUtil.bcryptHash(plainPassword)
    }

    @Throws(java.lang.Exception::class)
    fun verifyPassword(plainPassword: String): Boolean {
        return try {
            // LOG.info("Pwd \"${password}\"")
            val rawPassword: Password = ModularCrypt.decode(password)
            val factory = PasswordFactory.getInstance(BCryptPassword.ALGORITHM_BCRYPT)
            val restored = factory.translate(rawPassword) as BCryptPassword
            factory.verify(restored, plainPassword.toCharArray())
        } catch (E: Exception) {
            false
        }
    }

    companion object {
        private val LOG: Logger = Logger.getLogger(User::class.java)
    }
}

@ApplicationScoped
class UserRepository : PanacheRepositoryBase<User, UUID> {

    fun findByEmail(email: String) = find("email", email).firstResult<User>()

}
