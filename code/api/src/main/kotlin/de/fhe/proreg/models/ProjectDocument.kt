package de.fhe.proreg.models

import java.io.InputStream
import javax.ws.rs.FormParam
import javax.ws.rs.core.MediaType
import org.jboss.resteasy.annotations.providers.multipart.PartType

data class ProjectDocumentCreationRequest(

    @FormParam("title")
    @PartType(MediaType.TEXT_PLAIN)
    var title: String,

    @FormParam("content")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    var content: InputStream
)
