package de.fhe.proreg.models

import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.util.UUID

data class ProjectCreationRequest(
    var studentID: UUID,
    var supervisingTutor1ID: UUID,
    var supervisingTutor2ID: UUID?,
    var title: String,
    var studentSignature: String
)

data class ProjectViewResponse(
    var projectID: UUID,
    var studentID: UUID,
    var creationInstant: Instant,
    var modificationInstant: Instant,
    var supervisingTutor1ID: UUID,
    var supervisingTutor2ID: UUID?,
    var title: String,
    var grade: BigDecimal,
    var deadline: LocalDate,
    var supervisingTutor1Accepted: Boolean,
    var supervisingTutor2Accepted: Boolean,
    var studentSignature: String,
    var supervisingTutor1Signature: String,
    var supervisingTutor2Signature: String,
)

data class ProjectListResponse(
    var projects: List<ProjectViewResponse>
)
