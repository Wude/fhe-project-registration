package de.fhe.proreg.data

import java.time.Instant
import java.util.UUID

import javax.persistence.Cacheable
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ForeignKey
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.PrePersist
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import javax.enterprise.context.ApplicationScoped

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase

@Entity
@Cacheable
@Table(name = "project_documents", uniqueConstraints = [UniqueConstraint(name = "uk_project_documents", columnNames = ["title"])])
open class ProjectDocument {

    @Id
    @GeneratedValue
    @Column(name = "project_document_id", nullable = false, updatable = false)
    open var projectDocumentID: UUID? = null

    @Column(name = "creation_instant", nullable = false)
    lateinit open var creationInstant: Instant

    @Column(name = "modification_instant", nullable = false)
    lateinit open var modificationInstant: Instant

    @ManyToOne(optional = false, cascade = [CascadeType.ALL])
    @JoinColumn(name = "project_id", nullable = false, foreignKey = ForeignKey(name = "fk_projects_document_p_id"))
    lateinit open var project: Project

    @Column(name = "title", nullable = false)
    lateinit open var title: String

    @Column(name = "content", nullable = false)
    lateinit open var content: ByteArray

    @PrePersist
    fun onPrePersist() {
        if (projectDocumentID == null) {
            projectDocumentID = UUID.randomUUID()
            creationInstant = Instant.now()
        }
        modificationInstant = Instant.now()
    }

}

@ApplicationScoped
class ProjectDocumentRepository : PanacheRepositoryBase<ProjectDocument, UUID> {

}
