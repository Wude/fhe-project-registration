package de.fhe.proreg.resources

import de.fhe.proreg.models.BaseResponse
import java.io.IOException
import java.io.InputStream
import java.io.ByteArrayOutputStream
import javax.ws.rs.core.Response

abstract class ResourceBase {

    @Throws(IOException::class)
    protected fun getByteArray(inputStream: InputStream): ByteArray
    {
        val buffer = ByteArrayOutputStream()

        var readCount: Int = 0
        var byteArray = ByteArray(4)

        while ({ readCount = inputStream.read(byteArray, 0, byteArray.size); readCount }() != -1) {
            buffer.write(byteArray, 0, readCount)
        }

        buffer.flush()
        return buffer.toByteArray()
    }


    protected fun createResponse(status: Int, reason: String) : Response =
        Response.ok(BaseResponse(status, reason)).status(status, reason).build()

    protected fun createResponse(status: Int) : Response =
        Response.status(status).build()

    protected fun <T> createResponse(content: T, status: Int, reason: String) : Response =
        Response.ok(content).status(status, reason).build()

    protected fun <T> createResponse(content: T, status: Int) : Response =
        Response.ok(content).status(status).build()

}
