package de.fhe.proreg.services

import de.fhe.proreg.data.Project
import de.fhe.proreg.data.ProjectRepository
import de.fhe.proreg.data.ProjectStatus
import de.fhe.proreg.data.ProjectDocument
import de.fhe.proreg.data.ProjectDocumentRepository
import de.fhe.proreg.data.User
import de.fhe.proreg.data.UserGroup
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.data.UserType
import io.smallrye.jwt.build.Jwt
import io.quarkus.redis.client.reactive.ReactiveRedisClient
import io.smallrye.mutiny.Uni
import io.vertx.mutiny.redis.client.Response
import java.math.BigDecimal
import java.time.Instant
import java.time.Duration
import java.time.LocalDate
import java.time.ZoneId
import java.util.UUID
import javax.inject.Inject
import javax.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claims
import org.jboss.logging.Logger

/**
* Service for handling projects and project documents
*/
@ApplicationScoped
open class ProjectService {

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    lateinit var projectRepository: ProjectRepository

    @Inject
    lateinit var projectDocumentRepository: ProjectDocumentRepository

    fun createProject(
        studentID: UUID,
        supervisingTutor1ID: UUID,
        supervisingTutor2ID: UUID?,
        title: String,
        studentSignature: ByteArray
    ): String? {
        val student = userRepository.findById(studentID)
        val supervisingTutor1 = userRepository.findById(supervisingTutor1ID)
        val supervisingTutor2: User? = if (supervisingTutor2ID == null ) {
            null
        } else {
            userRepository.findById(supervisingTutor2ID)
        }

        if (student == null) {
            return "The student could not be found"
        } else if (supervisingTutor1 == null) {
            return "The first supervising tutor (lecturer) could not be found"
        } else if (supervisingTutor1.userType != UserType.LECTURER) {
            return "The user to be first supervising tutor must be a lecturer"
        } else {
            var project = Project()
            project.student = student
            project.supervisingTutor1 = supervisingTutor1
            if (supervisingTutor2 != null) {
                if (supervisingTutor2.userType != UserType.LECTURER) {
                    return "The user to be first supervising tutor must be a lecturer"
                }
                project.supervisingTutor2 = supervisingTutor2
            } else {
                project.supervisingTutor2Accepted = true
            }

            project.title = title
            project.deadline = LocalDate.ofInstant(Instant.now(), ZoneId_Germany)
            project.studentSignature = studentSignature
            project.status = ProjectStatus.SUBMITTED
            projectRepository.persist(project)

            return null
        }
    }

    fun viewProject(projectID: UUID, userID: UUID, userGroups: Set<String>): Pair<Project?, String?> {
        val project = projectRepository.findById(projectID)

        return if (project == null) {
            null to "A project with the given id does not exist"
        } else if (userID.equals(project.student.userID) ||
            userID.equals(project.supervisingTutor1.userID) ||
            userID.equals(project.supervisingTutor2?.userID) ||
            userGroups.contains(UserGroup.FACULTY_BOARD_OF_EXAMINERS) ||
            userGroups.contains(UserGroup.FACULTY_SECRETARIAT_STAFF) ||
            userGroups.contains(UserGroup.EXAMINATION_AUTHORITY_STAFF) ||
            userGroups.contains(UserGroup.ADMINISTRATOR))
        {
            project to null
        } else {
            null to "You may not access this project"
        }
    }

    fun viewProjectsByStudent(studentID: UUID): List<Project> = projectRepository.findByStudentID(studentID)

    fun viewProjectsByLecturer(lecturerID: UUID): List<Project> {
        val projects = mutableListOf<Project>()
        projects.addAll(projectRepository.findBySupervisingTutor1ID(lecturerID))
        projects.addAll(projectRepository.findBySupervisingTutor2ID(lecturerID))
        return projects
    }

    fun viewProjectsByUser(userID: UUID, groups: Set<String>): List<Project>? =
        if (groups.contains(UserGroup.STUDENT)) { viewProjectsByStudent(userID) }
        else if (groups.contains(UserGroup.LECTURER)) { viewProjectsByLecturer(userID) }
        else { null }

    fun viewProjectsByStatus(projectStatus: ProjectStatus, facultyAcronym: String): List<Project> =
        projectRepository.viewProjectsByStatus(projectStatus, facultyAcronym)

    fun viewProjectsByStatus(projectStatus: ProjectStatus): List<Project> =
        projectRepository.viewProjectsByStatus(projectStatus)

    fun viewProjectsByFaculty(facultyAcronym: String): List<Project> = projectRepository.findByFacultyAcronym(facultyAcronym)

    fun viewProjectsByDepartment(departmentAcronym: String): List<Project> = projectRepository.findByDepartmentAcronym(departmentAcronym)

    fun createProjectDocument(projectID: UUID, title: String, content: ByteArray): Boolean {
        val project = projectRepository.findById(projectID)
        return if (project == null) {
            false
        } else {
            var projectDocument = ProjectDocument()
            projectDocument.project = project
            projectDocument.title = title
            projectDocument.content = content
            projectDocumentRepository.persist(projectDocument)

            true
        }
    }

    // fun rejectProject(projectID: UUID, supervisingTutorID: UUID): Boolean {
    //     val project = projectRepository.findById(projectID)
    //     val supervisingTutor = userRepository.findById(supervisingTutorID)
    //     return if (project == null || supervisingTutor == null) {
    //         false
    //     } else {
    //         project.status = ProjectStatus.REJECTED
    //         projectRepository.persist(project)

    //         true
    //     }
    // }

    fun acceptProject(projectID: UUID, supervisingTutorID: UUID): Boolean {
        val project = projectRepository.findById(projectID)
        val supervisingTutor = userRepository.findById(supervisingTutorID)
        return if (project == null || supervisingTutor == null) {
            false
        } else {
            if (supervisingTutor.userID!!.equals(project.supervisingTutor1.userID)) {
                project.supervisingTutor1Accepted = true
            } else if (supervisingTutor.userID!!.equals(project.supervisingTutor2?.userID)) {
                project.supervisingTutor2Accepted = true
            }
            if (project.supervisingTutor1Accepted && project.supervisingTutor2Accepted) {
                project.status = ProjectStatus.ACCEPTED
            }
            projectRepository.persist(project)

            true
        }
    }

    fun changeProjectStatus(projectID: UUID, projectStatus: ProjectStatus): Boolean {
        val project = projectRepository.findById(projectID)
        return if (project == null) {
            false
        } else {
            project.status = projectStatus
            projectRepository.persist(project)

            true
        }
    }

    // fun requestDeadlineExtension(projectID: UUID): Boolean {
    //     val project = projectRepository.findById(projectID)
    //     return if (project == null) {
    //         false
    //     } else {
    //         project.status = ProjectStatus.DEADLINE_EXTENSION_REQUESTED
    //         projectRepository.persist(project)

    //         true
    //     }
    // }

    // fun grantDeadlineExtension(projectID: UUID): Boolean {
    //     val project = projectRepository.findById(projectID)
    //     return if (project == null) {
    //         false
    //     } else {
    //         project.status = ProjectStatus.DEADLINE_EXTENSION_GRANTED
    //         projectRepository.persist(project)

    //         true
    //     }
    // }

    fun gradeProject(projectID: UUID, grade: BigDecimal): Boolean {
        val project = projectRepository.findById(projectID)
        return if (project == null) {
            false
        } else {
            project.grade = grade
            project.status = ProjectStatus.GRADED
            projectRepository.persist(project)

            true
        }
    }

    // fun completeProject(projectID: UUID): Boolean {
    //     val project = projectRepository.findById(projectID)
    //     return if (project == null) {
    //         false
    //     } else {
    //         project.status = ProjectStatus.COMPLETED
    //         projectRepository.persist(project)

    //         true
    //     }
    // }

    companion object {
        val ZoneId_Germany: ZoneId = ZoneId.of("Europe/Berlin")

        private val LOG: Logger = Logger.getLogger(ProjectService::class.java)
    }
}
