package de.fhe.proreg.data

import de.fhe.proreg.Util
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
import java.time.Instant
import javax.persistence.Cacheable
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ForeignKey
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.OneToMany
import javax.persistence.PrePersist
import javax.persistence.Table
import javax.enterprise.context.ApplicationScoped

@Entity
@Cacheable
@Table(name = "faculties")
open class Faculty {

    @Id
    @Column(name = "faculty_acronym", columnDefinition = "char(3)", nullable = false, updatable = false)
    open var acronym: String? = null

    @Column(name = "creation_instant", nullable = false)
    open var creationInstant: Instant = Instant.ofEpochSecond(0)

    @Column(name = "modification_instant", nullable = false)
    lateinit open var modificationInstant: Instant

    @Column(name = "name", nullable = false)
    lateinit open var name: String

    @OneToMany(targetEntity = Department::class, orphanRemoval = true, mappedBy = "faculty")
    lateinit open var departments: List<Department>

    @OneToMany(targetEntity = Project::class, orphanRemoval = true, mappedBy = "faculty")
    lateinit open var projects: List<Project>

    @ManyToMany(targetEntity = User::class, cascade = [CascadeType.ALL])
    // @JoinTable(name = "user_faculty",
    //     inverseForeignKey = ForeignKey(name = "fk_user_faculty_f_acronym"))
    lateinit open var users: List<User>

    @PrePersist
    fun onPrePersist() {
        if (creationInstant.equals(Util.EpochStartInstant)) {
            creationInstant = Instant.now()
        }
        modificationInstant = Instant.now()
    }

}

@ApplicationScoped
class FacultyRepository : PanacheRepositoryBase<Faculty, String> {

}
