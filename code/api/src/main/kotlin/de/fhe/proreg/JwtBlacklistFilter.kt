package de.fhe.proreg

import de.fhe.proreg.services.UserService
import java.io.IOException
import javax.annotation.Priority
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Instance
import javax.inject.Inject
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerRequestFilter
import javax.ws.rs.container.PreMatching
import javax.ws.rs.core.Response
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.ext.Provider
import javax.ws.rs.Priorities
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.jboss.logging.Logger

@Provider
@PreMatching
@Priority(Priorities.USER)
@ApplicationScoped
class JwtBlacklistFilter : ContainerRequestFilter {

    @Inject
    @ConfigProperty(name = "quarkus.security.users.file.realm-name")
    lateinit var realm: Instance<String>

    @Inject
    lateinit var userService: UserService

    /**
     * Abort the filter chain with a 401 status code response.
     * 
     * <p>
     * The WWW-Authenticate header is sent along with the response.
     * </p>
     * 
     * @param requestContext The request context to use.
     */
    private fun abortUnauthorized(requestContext: ContainerRequestContext) {
        requestContext.abortWith(
            Response.status(Response.Status.UNAUTHORIZED).header(HttpHeaders.WWW_AUTHENTICATE,
                AUTHORIZATION_SCHEME + " realm=\"" + realm + "\"").build())
    }

    @Throws(IOException::class)
    public override fun filter(requestContext: ContainerRequestContext) {
        val authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION)
        if (authorizationHeader != null) {
            val authorizationHeaderParts: List<String> = authorizationHeader.split(" ")
            if (authorizationHeaderParts.size == 2 &&
                authorizationHeaderParts[0].equals(AUTHORIZATION_SCHEME, true) &&
                userService.tokenIsBlacklisted(authorizationHeaderParts[1]))
            {
                LOG.warn("Token blacklisted")
                abortUnauthorized(requestContext)
            }
        }
    }

    companion object {
        public const val AUTHORIZATION_SCHEME = "Bearer"

        private val LOG: Logger = Logger.getLogger(JwtBlacklistFilter::class.java)
    }
}
