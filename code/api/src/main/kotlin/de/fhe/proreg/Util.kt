package de.fhe.proreg

import java.time.Instant

object Util {
    val EpochStartInstant = Instant.ofEpochSecond(0)
}
