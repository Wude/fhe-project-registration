package de.fhe.proreg.models

import de.fhe.proreg.data.UserType
import java.util.UUID

data class UserLoginRequest(
    var email: String,
    var password: String
)

data class UserLoginResponse(
    var userID: UUID,
    var token: String
)

data class UserCreationRequest(
    var email: String,
    var password: String,
    var userType: UserType,
    var admin: Boolean,
    var facultyBoardOfExaminers: Boolean,
    var examinationAuthority: Boolean,
    var facultyAcronyms: List<String>,
    var departmentAcronyms: List<String>
)

data class UserPasswordChangeRequest(
    var password: String
)

data class UserViewResponse(
    var userID: UUID,
    var email: String,
    var password: String,
    var userType: UserType,
    var admin: Boolean,
    var facultyBoardOfExaminers: Boolean,
    var examinationAuthority: Boolean,
    var facultyAcronyms: List<String>,
    var departmentAcronyms: List<String>
)
