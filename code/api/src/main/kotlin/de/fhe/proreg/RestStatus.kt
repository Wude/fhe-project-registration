package de.fhe.proreg

object RestStatusInt {
    const val OK_200 = 200
    const val CREATED_201 = 201
    const val NO_CONTENT_204 = 204
    const val BAD_REQUEST_400 = 400
    const val UNAUTHORIZED_401 = 401
}

object RestStatusString {
    const val OK_200 = "200"
    const val CREATED_201 = "201"
    const val NO_CONTENT_204 = "204"
    const val BAD_REQUEST_400 = "400"
    const val UNAUTHORIZED_401 = "401"
}
