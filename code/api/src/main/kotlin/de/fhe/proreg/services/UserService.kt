package de.fhe.proreg.services

import de.fhe.proreg.data.Department
import de.fhe.proreg.data.DepartmentRepository
import de.fhe.proreg.data.Faculty
import de.fhe.proreg.data.FacultyRepository
import de.fhe.proreg.data.User
import de.fhe.proreg.data.UserGroup
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.data.UserType
import io.smallrye.jwt.build.Jwt
import io.smallrye.mutiny.Uni
import io.quarkus.redis.client.reactive.ReactiveRedisClient
import io.vertx.mutiny.redis.client.Response
import java.util.UUID
import java.time.Instant
import java.time.Duration
import javax.inject.Inject
import javax.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claims
import org.jboss.logging.Logger

@ApplicationScoped
open class UserService {

    @Inject
    @ConfigProperty(name = "mp.jwt.verify.issuer", defaultValue = "https://fh-erfurt.de/proreg")
    lateinit var issuer: String

    @Inject
    @ConfigProperty(name = "de.fhe.proreg.token-validity-seconds", defaultValue = "3600")
    var tokenValiditySeconds: Long = 3600

    val redisAwaitDuration = Duration.ofSeconds(60)

    @Inject
    lateinit var redisClient: ReactiveRedisClient

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    lateinit var facultyRepository: FacultyRepository

    @Inject
    lateinit var departmentRepository: DepartmentRepository

    private fun getFaculties(facultyAcronyms: List<String>): List<Faculty> {
        val faculties = mutableListOf<Faculty>()
        for (facultyAcronym in facultyAcronyms) {
            var faculty = facultyRepository.findById(facultyAcronym)
            if (faculty != null) {
                faculties.add(faculty)
            }
        }
        return faculties
    }

    private fun getDepartments(departmentAcronyms: List<String>): List<Department> {
        val departments = mutableListOf<Department>()
        for (departmentAcronym in departmentAcronyms) {
            var department = departmentRepository.findById(departmentAcronym)
            if (department != null) {
                departments.add(department)
            }
        }
        return departments
    }

    fun createUser(
        email: String,
        password: String,
        userType: UserType,
        admin: Boolean,
        facultyBoardOfExaminers: Boolean,
        examinationAuthority: Boolean,
        facultyAcronyms: List<String>,
        departmentAcronyms: List<String>
    ): Boolean {
        var user = userRepository.findByEmail(email)
        return if (user == null) {
            user = User()
            user.email = email
            user.password = password
            user.userType = userType
            user.admin = admin
            user.facultyBoardOfExaminers = facultyBoardOfExaminers
            user.examinationAuthority = examinationAuthority
            user.faculties = getFaculties(facultyAcronyms)
            user.departments = getDepartments(departmentAcronyms)
            userRepository.persist(user)

            true
        } else {
            false
        }
    }

    fun changePassword(email: String, password: String): Boolean {
        var user = userRepository.findByEmail(email)
        return if (user != null) {
            user.changePassword(password)
            userRepository.persist(user)

            true
        } else {
            false
        }
    }

    fun getGroups(user: User): Set<String> {
        var groups = mutableSetOf(UserType.STUDENT.code)
        if (user.admin) { groups.add(UserGroup.ADMINISTRATOR) }
        if (user.facultyBoardOfExaminers) { groups.add(UserGroup.FACULTY_BOARD_OF_EXAMINERS) }
        return groups
    }

    fun createToken(email: String, plainPassword: String): Pair<UUID?, String?> {
        val user = userRepository.findByEmail(email)
        return if (user == null || user.verifyPassword(plainPassword)) {
            null to null
        } else {
            user.userID to Jwt.issuer(issuer)
                .upn(user.email)
                .groups(getGroups(user))
                .claim(Claims.sub.name, user.userID)
                .claim(Claims.email.name, user.email)
                .claim(Claims.email_verified.name, user.email)
                .issuedAt(Instant.now())
                .expiresIn(Duration.ofMinutes(tokenValiditySeconds))
                .sign()
        }
    }

    fun tokenIsBlacklisted(token: String): Boolean {
        val response = redisClient.get("token:blacklist:${token}").await().atMost(redisAwaitDuration)

        return if (response == null) {
            false
        } else {
            val result = response.toString()
            result.equals("OK")
        }
    }

    fun revokeToken(token: String) {
        redisClient.set(listOf("token:blacklist:${token}", "OK")).await().atMost(redisAwaitDuration)
    }

    companion object {
        private val LOG: Logger = Logger.getLogger(UserService::class.java)
    }
}
