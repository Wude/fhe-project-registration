package de.fhe.proreg.data

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
import java.time.Instant
import java.time.LocalDate
import java.util.UUID
import java.math.BigDecimal
import javax.persistence.AttributeConverter
import javax.persistence.Cacheable
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Converter
import javax.persistence.Entity
import javax.persistence.ForeignKey
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.PrePersist
import javax.persistence.Table
import javax.enterprise.context.ApplicationScoped

enum class ProjectStatus(val code: String) {
    SUBMITTED("SUB"),
    REJECTED("REJ"),
    ACCEPTED("ACC"),
    REGISTERED("REG"),
    DEADLINE_EXTENSION_REQUESTED("DEQ"),
    DEADLINE_EXTENSION_REJECTED("DEJ"),
    DEADLINE_EXTENSION_GRANTED("DEG"),
    GRADED("GRA"),
    COMPLETED("COM");

    companion object {

        val valuesByCode = HashMap<String, ProjectStatus>()

        init {
            val projectStatusValues = ProjectStatus.values()

            for (projectStatus in projectStatusValues) {
                valuesByCode.put(projectStatus.code, projectStatus);
                valuesByCode.put(projectStatus.name, projectStatus);
            }
        }

        fun tryValueOf(idOrCode: String?, defaultValue: ProjectStatus?): ProjectStatus? = valuesByCode.get(idOrCode) ?: defaultValue
        fun tryValueOf(idOrCode: String?): ProjectStatus? = tryValueOf(idOrCode, null)
    }
}

@Converter(autoApply = true)
class ProjectStatusConverter : AttributeConverter<ProjectStatus, String> {

    override fun convertToDatabaseColumn(projectStatus: ProjectStatus?): String? = if (projectStatus == null) { null } else { projectStatus.code }
    override fun convertToEntityAttribute(code: String?): ProjectStatus? = ProjectStatus.tryValueOf(code)
}

@Entity
@Cacheable
@Table(name = "projects")
open class Project {

    @Id
    @GeneratedValue
    @Column(name = "project_id", nullable = false, updatable = false)
    open var projectID: UUID? = null

    @Column(name = "creation_instant", nullable = false)
    lateinit open var creationInstant: Instant

    @Column(name = "modification_instant", nullable = false)
    lateinit open var modificationInstant: Instant

    @ManyToOne(optional = false, cascade = [CascadeType.ALL])
    @JoinColumn(name = "student_id", nullable = false, foreignKey = ForeignKey(name = "fk_projects_student_id"))
    lateinit open var student: User

    @ManyToOne(optional = false, cascade = [CascadeType.ALL])
    @JoinColumn(name = "supervising_tutor_1_id", nullable = false, foreignKey = ForeignKey(name = "fk_projects_tutor_1_id"))
    lateinit open var supervisingTutor1: User

    @ManyToOne(optional = true, cascade = [CascadeType.ALL])
    @JoinColumn(name = "supervising_tutor_2_id", nullable = true, foreignKey = ForeignKey(name = "fk_projects_tutor_2_id"))
    open var supervisingTutor2: User? = null

    @Column(name = "supervising_tutor_1_accepted", nullable = false)
    open var supervisingTutor1Accepted: Boolean = false

    @Column(name = "supervising_tutor_2_accepted", nullable = false)
    open var supervisingTutor2Accepted: Boolean = false

    @Column(name = "student_signature", nullable = true)
    lateinit open var studentSignature: ByteArray

    @Column(name = "supervising_tutor_1_signature", nullable = true)
    lateinit open var supervisingTutor1Signature: ByteArray

    @Column(name = "supervising_tutor_2_signature", nullable = true)
    lateinit open var supervisingTutor2Signature: ByteArray

    @Column(name = "title", nullable = false)
    lateinit open var title: String

    @Column(name = "grade", nullable = true, precision = 2, scale = 1)
    lateinit open var grade: BigDecimal

    @Column(name = "deadline", nullable = false)
    lateinit open var deadline: LocalDate

    @Convert(converter = ProjectStatusConverter::class)
    @Column(name = "status", nullable = false, columnDefinition = "char(1)")
    lateinit open var status: ProjectStatus

    @OneToMany(targetEntity = ProjectDocument::class, orphanRemoval = true, mappedBy = "project")
    lateinit open var documents: List<ProjectDocument>

    @ManyToOne(optional = false, cascade = [CascadeType.ALL])
    @JoinColumn(name = "faculty_acronym", nullable = false, foreignKey = ForeignKey(name = "fk_projects_f_acronym"))
    lateinit open var faculty: Faculty

    @ManyToOne(optional = false, cascade = [CascadeType.ALL])
    @JoinColumn(name = "department_acronym", nullable = false, foreignKey = ForeignKey(name = "fk_projects_d_acronym"))
    lateinit open var department: Department

    @PrePersist
    fun onPrePersist() {
        if (projectID == null) {
            projectID = UUID.randomUUID()
            creationInstant = Instant.now()
        }
        modificationInstant = Instant.now()
    }

}

@ApplicationScoped
class ProjectRepository : PanacheRepositoryBase<Project, UUID> {

    fun findByStudentID(studentID: UUID) = find("studentID", studentID).list<Project>()

    fun findBySupervisingTutor1ID(lecturerID: UUID) = find("supervisingTutor1ID", lecturerID).list<Project>()

    fun findBySupervisingTutor2ID(lecturerID: UUID) = find("supervisingTutor2ID", lecturerID).list<Project>()

    // fun findByUserId(userID: UUID): List<Project> {
    //     var projects = find("studentID", userID).list<Project>()
    //     if (projects.size == 0) {
    //         projects = find("lecturerID", userID).list<Project>()
    //     }
    //     return projects
    // }

    fun viewProjectsByStatus(projectStatus: ProjectStatus, facultyAcronym: String) =
        find("projectStatus = :projectStatus and facultyAcronym = :facultyAcronym", projectStatus, facultyAcronym).list<Project>()

    fun viewProjectsByStatus(projectStatus: ProjectStatus) = find("projectStatus", projectStatus).list<Project>()

    fun findByFacultyAcronym(facultyAcronym: String) = find("facultyAcronym", facultyAcronym).list<Project>()

    fun findByDepartmentAcronym(departmentAcronym: String) = find("departmentAcronym", departmentAcronym).list<Project>()

}
