# Workflow system for student projects and theses

A summary in german is "Workflowsystem Studentische Arbeiten".

## Docker-Container configuration

Production & Development:
* `gateway.yml`: Traefik as reverse-proxy or load balancer
* `api-cache.yml`: Redis
* `api-ab.yml`: Postgres
* `api.yml`: The project registration API with its source in `./code/api`

Development:
* `db-admin.yml`: pgadmin4

Production only:
* `logger.yml`: Graylog (with Mongo and Elasticsearch)
* `tracer.yml`: Jaegertracing
* `monitor.yml`: Prometheus
* `monitor-ui.yml`: Grafana

## Deployment

_*Important*_: All commands must be executed in the project base path.
The following commands must be used for start and shutdown.

### Development

Quarkus-dev-service:
* VS-Code-task: `code/api: quarkus:dev`
* bash-script: `./mvnw quarkus:dev`
* cmd-script: `mvnw.cmd quarkus:dev`

Start:
* VS-Code-task: `docker/dev: up`
* bash-script: `./docker/dev.sh up`
* cmd-script: `docker\dev.bat up`

Shutdown:
* VS-Code-task: `docker/dev: down`
* bash-script: `./docker/dev.sh down`
* cmd-script: `docker\dev.bat down`

### Production

Compilation:
* VS-Code-task: `code/api: clean install`
* bash-script: `./mvnw -Dmaven.test.skip=true clean install`
* cmd-script: `mvnw.cmd -Dmaven.test.skip=true clean install`

Start:
* VS-Code-task: `docker/prod: up`
* bash-script: `./docker/prod.sh up`
* cmd-script: `docker\prod.bat up`

Shutdown:
* VS-Code-task: `docker/prod: down`
* bash-script: `./docker/prod.sh down`
* cmd-script: `docker\prod.bat down`

## Access

There are two users registered:
* `steffen.avemarg@fh-erfurt.de`
* `stefan.woyde@fh-erfurt.de`

Both users are admins and their password for is initially `123`.
