@docker-compose ^
    --env-file .env ^
    -f ./docker/config/gateway.yml ^
    -f ./docker/config/api-cache.yml ^
    -f ./docker/config/api-db.yml ^
    -f ./docker/config/api.yml ^
    %*

    @REM -f ./docker/config/logger.yml ^
    @REM -f ./docker/config/tracer.yml ^
    @REM -f ./docker/config/monitor.yml ^
    @REM -f ./docker/config/monitor-ui.yml ^
