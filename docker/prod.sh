#!/bin/bash
set -e

docker-compose \
    -f ./docker/config/gateway.yml \
    -f ./docker/config/api-cache.yml \
    -f ./docker/config/api-db.yml \
    -f ./docker/config/api.yml \
    $@

    # -f ./docker/config/logger.yml \
    # -f ./docker/config/tracer.yml \
    # -f ./docker/config/monitor.yml \
    # -f ./docker/config/monitor-ui.yml \
