#!/bin/bash
set -e

docker-compose \
    -f ./docker/config/gateway.yml \
    -f ./docker/config/api-cache.yml \
    -f ./docker/config/api-db.yml \
    -f ./docker/config/db-admin.yml \
    $@
