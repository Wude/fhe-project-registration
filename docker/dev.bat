@docker-compose ^
    --env-file .env ^
    -f ./docker/config/gateway.yml ^
    -f ./docker/config/api-cache.yml ^
    -f ./docker/config/api-db.yml ^
    %*
    @REM -f ./docker/config/db-admin.yml ^
