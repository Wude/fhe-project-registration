# Implementation

![Components](../images/component-overview.svg)

## Redis

Currently used for storing blacklisted jwt tokens.

## API: Services & REST-Resources

* `UserService` & `UserResource`: user persistence and login/logout
* `FacultyService` & `FacultyResource`: faculty and department persistence
* `ProjectService` & `ProjectResource`: project and project document persistence
